#!/usr/bin/env bash

#kill all processes of manage.py
pkill -9 -f manage.py
#give time for processes to stop
echo "Sleeping fo 15 seconds to allows threads to stop"
sleep 15
#clear out logs. Not error log
echo "Clearing info log"
>| ./log/python.log
bash ./start_server.sh
