__author__ = 'dabslants'

from flask import Flask
from flask_assets import Environment
from flask_bootstrap import Bootstrap
from flask_mail import Mail
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy

from config import config
from .util.assets import bundles

bootstrap = Bootstrap()
mail = Mail()
moment = Moment()
db = SQLAlchemy()


# login manger for user session
# login_manager = LoginManager()
# login_manager.session_protection = 'strong'
# login_manager.login_view = "/login"
# login_manager.anonymous_user = Anonymous


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    bootstrap.init_app(app)
    mail.init_app(app)
    moment.init_app(app)
    db.init_app(app)

    # register blueprints
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint, url_prefix='/')

    assets = Environment(app)
    assets.register(bundles)

    return app
