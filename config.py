__author__ = 'dabslants'

import os

BASE_DIR = os.path.abspath(os.path.dirname(__file__))


class Config:
    DATABASE_LOCAL_URL = 'mysql+pymysql://<user>:<pass>@localhost/pyprog1'

    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # SQLALCHEMY_POOL_RECYCLE = 45

    @staticmethod
    def init_app(app):
        pass


class Development(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or Config.DATABASE_LOCAL_URL


class Production(Config):
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or Config.DATABASE_LOCAL_URL


config = {
    'development': Development,
    'production': Production,
    'default': Development
}
