# To deploy this python project

1. install python3.5

2. go to project package

for linux
```commandline
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

for windows
```commandline
python3 -m venv venv
venv\Scripts\activate
pip install -r requirements.txt
```

3. deploy and run the server
```mysql-sql
create database pyprog1;
GRANT ALL PRIVILEGES ON pyprog1.* TO 'pyprog1'@'%';
```

```commandline
python3 manage.py db init
python3 manage.py db migrate
python3 manage.py db upgrade
python3 manage.py deploy

python3 manage.py runserver

or
./deploy.sh
./start.sh
./restart.sh
```

4. install httpie for testing
```commandline
pip install httpie

http --auth user:pass GET http://localhost:5010/path-to-script any-param-and-val-required
```
